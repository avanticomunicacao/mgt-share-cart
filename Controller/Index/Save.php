<?php
namespace Avanti\ShareCart\Controller\Index;

use Avanti\ShareCart\Model\ShareCartFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Math\Random as RandomHash;
use Magento\Framework\UrlInterface;

class Save extends Action
{
    /**
     * @var ShareCartFactory
     */
    private $shareCartFactory;

    /**
     * @var JsonFactory
     */
    protected $jsonFactory;

    /**
     * @var RandomHash
     */
    protected $randomHash;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;



    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        CheckoutSession $checkoutSession,
        RandomHash $randomHash,
        ShareCartFactory $shareCartFactory
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->jsonFactory  = $jsonFactory;
        $this->randomHash = $randomHash;
        $this->shareCartFactory = $shareCartFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        try{
            $shareCart = $this->shareCartFactory->create();
            $cartItems = $this->checkoutSession->getQuote()->getAllVisibleItems();

            $productI = array();

            foreach ($cartItems as $cartItem) {
                if ($cartItem->getProductType() === 'configurable') {
                    $options = $cartItem->getProduct()->getTypeInstance(true)->getOrderOptions($cartItem->getProduct());
                    $configurations = $options['attributes_info'];

                    foreach ($configurations as $configuration) {
                        $productI[] = [
                            "id" => $cartItem->getProductId(),
                            'qty' => $cartItem->getQty(),
                            'configurable_options' =>
                            [
                                "option_id" => $configuration['option_id'],
                                "option_value" => $configuration['option_value']
                            ]
                        ];
                    }
                } else {
                    $productI[] = [
                        "id" => $cartItem->getProductId(),
                        'qty' => $cartItem->getQty(),
                        ];
                }

            }

            $items = json_encode($productI);

            $randomHash = $this->randomHash->getUniqueHash();

            $shareCart->setData('products_items',$items);
            $shareCart->setData('expiration_date', '2021-01-08');
            $shareCart->setData('token', $randomHash);

            $shareCart->save();
        } catch (NoSuchEntityException $e) {
            $resultJson = $this->jsonFactory->create();
            $resultJson->setData(['success' => 'false', "mensagem" => "Não foi posível fazer o compartilhamento do carrinho"]);
            return $resultJson;
        }

        $url = $this->_url->getUrl("sharecart/index/index", ["token" => $randomHash]);

        $resultJson = $this->jsonFactory->create();
        $resultJson->setData(['success' => 'true', "mensagem" => __("Shared Cart created with success"), "url" => $url]);
        return $resultJson;
    }
}