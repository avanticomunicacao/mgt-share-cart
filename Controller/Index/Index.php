<?php
namespace Avanti\ShareCart\Controller\Index;

use Avanti\ShareCart\Model\ResourceModel\ShareCart\CollectionFactory as ShareCartCollectionFactory;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Model\ProductRepository;
use Magento\Store\Model\StoreManagerInterface;



class Index extends Action
{
    /**
     * @var ShareCartCollectionFactory
     */
    protected $shareCartCollectionFactory;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManagerInterface;

    /**
     * @var ProductRepository
     */
    protected $productRepository;


    protected $cart;

    public function __construct(
        Cart $cart,
        Context $context,
        CheckoutSession $checkoutSession,
        ProductRepository $productRepository,
        ShareCartCollectionFactory $shareCartCollectionFactory,
        StoreManagerInterface $storeManagerInterface
        )
    {
        $this->cart = $cart;
        $this->checkoutSession = $checkoutSession;
        $this->productRepository = $productRepository;
        $this->shareCartCollectionFactory = $shareCartCollectionFactory;
        $this->storeManagerInterface = $storeManagerInterface;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $token = $this->getRequest()->getParam('token');
            if (!$token) {
                throw new NoSuchEntityException(__("You must enter the shared cart token at the URL"));
            }

            $shareCart = $this->shareCartCollectionFactory->create();
            $collection = $shareCart->addFieldToFilter('token', $token)->getData();

            if (!$collection || !key_exists(0, $collection)) {
                throw new NoSuchEntityException(__("Shared cart token not found"));
            }
            $shareCart = $collection[0];
            $cartItems = json_decode($shareCart['products_items']);
            $this->checkoutSession->clearQuote();

            if (is_array($cartItems)) {
                $this->addProductsToCart($cartItems, true);
            } else {
                $this->addProductsToCart($cartItems, false);
            }
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addExceptionMessage($e);
            $redirect = $this->resultRedirectFactory->create();
            $redirect->setPath('checkout/cart');
            return $redirect;
        }

        $redirect = $this->resultRedirectFactory->create();
        $this->messageManager->addSuccessMessage(__("Cart Items add with success in your cart"));
        $redirect->setPath('checkout/cart');
        return $redirect;
    }


    private function addProductsToCart($items, $isArray)
    {
        $storeId = $this->getStoreId();

        if (!$isArray) {
            $product = $this->productRepository->getById($items->id, false, $storeId, true);

            if (key_exists('configurable_options', $items)) {
                $options = [
                    $items->configurable_options->option_id => $items->configurable_options->option_value
                ];

                $params = array(
                    'product' => $product->getId(),
                    'super_attribute' => $options,
                    'qty' => $items->qty
                );

                $this->cart->addProduct($product, $params);
            } else {
                $this->cart->addProduct($product, $items->qty);
            }
        } else {
            foreach ($items as $cartItem) {
                $product = $this->productRepository->getById($cartItem->id, false, $storeId, true);

                if (key_exists('configurable_options', $cartItem)) {
                    $options = [
                        $cartItem->configurable_options->option_id => $cartItem->configurable_options->option_value
                    ];

                    $params = array(
                        'product' => $product->getId(),
                        'super_attribute' => $options,
                        'qty' => $cartItem->qty
                    );

                    $this->cart->addProduct($product, $params);
                } else {
                    $this->cart->addProduct($product, $cartItem->qty);
                }
            }
        }

        $this->cart->save();
    }

    /**
     * @return int
     * @throws NoSuchEntityException
     */
    private function getStoreId()
    {
        return $this->storeManagerInterface->getStore()->getId();
    }
}