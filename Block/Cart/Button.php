<?php


namespace Avanti\ShareCart\Block\Cart;


use Magento\Framework\View\Element\Template;

class Button extends Template
{
    public function getAjaxUrl()
    {
        return $this->getUrl("sharecart/index/save");
    }
}