<?php
namespace Avanti\ShareCart\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class ShareCart extends AbstractModel implements IdentityInterface
{
    /**
     * Cache tag.
     */
    const CACHE_TAG = 'avanti_sharecart';


    protected $_cacheTag = 'avanti_sharecart';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'avanti_sharecart';


    /**
     * Initialize resource model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init("Avanti\ShareCart\Model\ResourceModel\ShareCart");
    }


    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        return [];
    }
}