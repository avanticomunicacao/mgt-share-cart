<?php
namespace Avanti\ShareCart\Model\ResourceModel\ShareCart;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init("Avanti\ShareCart\Model\ShareCart", "Avanti\ShareCart\Model\ResourceModel\ShareCart");
    }
}