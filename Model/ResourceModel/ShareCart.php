<?php
namespace Avanti\ShareCart\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ShareCart extends AbstractDb
{
    protected function _construct()
    {
        $this->_init("avanti_sharecart", "entity_id");
    }
}